module.exports = class users_storage {

  constructor() {
    console.log('connected to storage');

    this.users = [{id: 1, name: 'pavel', score: '100'},
      {id: 2, name: 'netology', score: '100'},
      {id: 3, name: 'nick', score: '100'},
      {id: 4, name: 'ann', score: '100'},
      {id: 5, name: 'ivan', score: '100'},
      {id: 6, name: 'rick', score: '100'}];
    this.nextId = 3;

    /*
    user = {
      id: integer,
      name: string,
      score: integer
    }

    array index is userID

     */
  }

  createNewUser(data) {
    console.log(`We will try to create new user ${data}`);

  }

  getUserList(fields, limit, offset) {
    let list = this.users;
    console.log(`We will try to send userlist`);
    if (fields || limit || offset) list = this.filterFields(list, fields, limit, offset);
    return list;

  }

  getUser(userId, fields) {
    console.log(`We will try to send user with id:${userId}`);

    let user_index = this.getUserIndexById(userId);

    if (user_index == undefined) return undefined;
    else if (fields) return this.filterFields(this.users[user_index], fields, 0, 0);
    else return this.users[user_index];

  }

  deleteUser(userId) {
    console.log(`We will try to delete user with id:${userId}`);
    let user_index = this.getUserIndexById(userId);
    if (user_index == undefined) return undefined;

    this.users.splice(user_index, 1);
    return user_index;

  }

  getUserIndexById(userId) {
    let user = this.users.find((element, index, array) => {
      return element.id == userId;
    });

    if (user == undefined) return undefined;
    else return this.users.indexOf(user);
  }

  updateUser(userId, data) {
    let user_index = this.getUserIndexById(userId);
    if (user_index == undefined) return undefined;

    for (let key in data) { //Or we can set each param like this.users[user_index].name = data.name;
      if (key == 'id') continue;
      this.users[user_index][key] = data[key];

    }

    return this.getUser(userId);

  }

  createUser(data) {
    //We will suggest POST requests will be not so often... We are not DB
    let newId = this.nextId++;
    console.log(`We will try to create user with id ${newId}`);
    console.log(data);

    this.users.push({id: newId});
    let user_index = this.getUserIndexById(newId);
    return this.updateUser(newId, data);

  }

  deleteAllUsers() {
    this.users = [];
    return true; //Can't imagine any error...
  }

  filterFields(result, fields, limit, offset) {
    let filteredResult = [];

    if (offset--) {
      limit = (!limit) ? result.length : limit;
      console.log(`result.slice(${offset},${offset + limit})`);
      result = result.slice(offset, offset + limit);
    }

    //Filter single result like array
    if (!Array.isArray(result)) result = [result];

    if (fields) {
      for (let userIndex in result) {
        let filteredFields = {};
        for (let key in result[userIndex]) { //sad, but  .filter don't work with Object
          if (fields.includes(key))
            filteredFields[key] = result[userIndex][key];
        }

        filteredResult.push(filteredFields);
      }
    }

    return filteredResult;
  }

};