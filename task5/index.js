const express = require("express");
const app = express();

const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({extended: false});
const jsonParser = bodyParser.json();

const userStorage = require('./users_handlers');

let users = new userStorage();

//REST
const REST_API = express.Router();

REST_API.param('id', (req, res, next, id) => {
  let userId = parseInt(id);
  console.log(`USER ID = ${userId} with type ${typeof userId}`);
  if (isNaN(userId)) res.status(400).send('Bad request. Check ID');

  req.id = userId;
  next();
});

REST_API.get("/users/", function (req, res) {
  let listLimit = req.query.limit || 0; //Will suggest limit=0 => no limit => display all
  let listOffset = req.query.offset || 1;
  let listFields = req.query.fields || false;
  console.log(`REST GET USERS LIST with limit ${listLimit} from position ${listOffset}`);

  let userlist = users.getUserList(listFields, listLimit, listOffset);
  // if (typeof userList == undefined) res.sendStatus(404);
  // Empty Array is normal result... just send []
  res.send(userlist);
});
REST_API.post("/users/", function (req, res) {
  console.log(`REST POST NEW USERS TO LIST`);
  console.log(req.body);
  if ((user = users.createUser(req.body)) == undefined) res.sendStatus(404);
  res.send(user);
});
REST_API.get("/users/:id", function (req, res) {
  let listFields = req.query.fields || false;
  console.log(`REST GET USER ${req.id} FROM LIST`);
  let user = users.getUser(req.id, listFields);
  if (user == null) res.sendStatus(404);
  res.send(user);

});
REST_API.put("/users/:id", function (req, res) {
  console.log(`REST UPDATE USER ${req.id} IN LIST with data: ${req.body}`);
  if ((user = users.updateUser(req.id, req.body)) == undefined) res.sendStatus(404);
  res.send(user);

});
REST_API.delete("/users/:id", function (req, res) {
  console.log(`REST DELETE USER ${req.id} FROM LIST`);

  if (users.deleteUser(req.id) == undefined) res.sendStatus(404);
  res.sendStatus(204);
});

REST_API.delete("/users/", function (req, res) {
  console.log(`REST DELETE ALL USERS FROM LIST`);

  if (users.deleteAllUsers()) res.sendStatus(204);
  else res.sendStatus(500);
});


// RPC
const RPC_API = express.Router();

RPC_API.post("/", function (req, res) {
  console.log('RPC POST HANDLED');

  if (!req.body) return res.json(RPCError('Parse error', -32700, null));
  let RPCData = req.body;
  if (!RPCData) {
    return res.json(RPCError('Parse error', -32700, null));
  }

  let apiMethod = RPCData.method;
  if (!apiMethod) return res.json(RPCError('Procedure not found', -32601, null));

  let reqiestId = RPCData.id || null;
  let userId = RPCData.params.id || null;
  let userData = RPCData.params.data || null;
  let listLimit = parseInt(RPCData.params.limit) || 0; //Will suggest limit=0 => no limit => display all
  let listOffset = parseInt(RPCData.params.offset) || 1;
  let listFields = RPCData.params.fields || false;

  switch (apiMethod) {
    case 'getUserList':
      let limit = RPCData.params.limit;
      console.log(`RPC GET USERS LIST with limit ${listLimit} from position ${listOffset}`);

      let userlist = users.getUserList(listFields, listLimit, listOffset);
      return res.json(RPCResult(userlist, reqiestId));
      break;

    case 'getUser':
      console.log(`RPC GET USER ${userId } FROM LIST`);
      let user = users.getUser(userId, listFields);
      if (user == null) return res.json(RPCError('User not found', 404, reqiestId));
      return res.json(RPCResult(user, reqiestId));
      break;
    case 'deleteUser':
      console.log(`RPC DELETE USER ${userId} FROM LIST`);
      if (users.deleteUser(userId) == undefined) return res.json(RPCError('User not found', 404, reqiestId));
      return res.json(RPCResult('user deleted', reqiestId));
      break;
    case 'deleteAllUsers':
      console.log(`RPC DELETE ALL USERS FROM LIST`);
      if (users.deleteAllUsers()) return res.json(RPCResult('all users deleted', reqiestId));
      else res.json(RPCError('Some internal error', 500, reqiestId));
      break;
    case 'updateUser':
      console.log(`RPC UPDATE USER ${userId} IN LIST with data: ${userData}`);
      let updatedUser = users.updateUser(userId, userData);
      if (updatedUser == undefined) return res.json(RPCError('User not found', 404, reqiestId));
      return res.json(RPCResult(updatedUser, reqiestId));
      break;
    case 'createUser':
      console.log(`RPC POST USER TO LIST with data: ${userData}`);
      let newUser = users.createUser(userData);
      if (newUser == undefined) return res.json(RPCError('Some error on creation', 404, reqiestId));
      return res.json(RPCResult(newUser, reqiestId));
      break;
    default:
      return res.json(RPCError('Not implemented', 400, reqiestId)); //501

  }
});


let RPCResult = (result, id) => {
  return {'jsonrpc': '2.0', 'result': result, 'id': id};
};

let RPCError = (error, code, id) => {
  return {'jsonrpc': '2.0', 'error': {'code': code || null, 'message': error}, 'id': id};
};


// GENERAL
app.use("/api/", urlencodedParser, REST_API);
app.use("/api/rpc", jsonParser, RPC_API);

app.listen(process.env.PORT || 3000);