const filename = process.argv[2];
const crypto = require('crypto');
const fs = require('fs');
const {Transform} = require('stream');

const hash = crypto.createHash('md5');

class hexTransform extends Transform {
  constructor(options) {
    super(options);
    this.hasher = crypto.createHash('md5');
  }

  _transform(chunk, encoding, cb) {
    this.hasher.update(chunk);
    cb();
  }

  _flush(cb) {

    this.push(this.hasher.digest('hex'));
    this.hasher = null;
    cb();
  };
}

const input = fs.createReadStream(filename);
const output = fs.createWriteStream('md5.txt');
/*console.log('PART 1. Read&Write Streams')
 input.pipe(hash)
 hash.pipe(output);
 hash.pipe(process.stdout);
 */
console.log('PART 2. Transform Stream');
const hextr = new hexTransform();
input.pipe(hextr);
hextr.pipe(process.stdout);
hextr.pipe(output);



