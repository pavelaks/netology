/*
Основное задание

Создать приложение на Express.js которое будет иметь 5 вариаций роутов:

GET / – Главная страница которая вернет код 200 OK и покажет текст "Hello, Express.js"
GET /hello – Страница, код ответа 200 OK и покажет текст "Hello stranger!"
GET /hello/[любое имя] – Страница, код ответа 200 OK и покажет текст "Hello, [любое имя]!"
ANY /sub/[что угодно]/[возможно даже так] – Любая из этих страниц должна показать текст "You requested URI: [полный URI запроса]"
POST /post – Страница которая вернет все тело POST запроса (POST body) в JSON формате, либо 404 Not Found - если нет тела запроса
Дополнительное задание

Добавить в роут POST /post проверку на наличие Header: Key (на уровне middleware), если такого header не существует, то возвращать 401
 */

const express = require("express");
const app = express();

const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

const checkHeaderKey = (req, res, next) => {
  if (req.header('Project') === 'Netology') next();
  else res.sendStatus(401);
};

const isEmpty = (obj) => {
  for (let key in obj) {
    if (obj.hasOwnProperty(key))
      return false;
  }
  return true;
};

app.get('/', (req, res) => {
  res.status(200).send(`Hello, Express.js`)
});
app.get('/hello', (req, res) => {
  res.status(200).send(`Hello stranger!`)
});
app.get('/hello/:username', (req, res) => {
  res.status(200).send(`Hello, ${req.params.username}!`)
});
app.use('/sub/*', (req, res) => {
  res.send(`You requested URI, ${req.protocol}://${req.get('host') + req.originalUrl}`)
});
app.post('/post', checkHeaderKey, jsonParser, (req, res) => {
  if (isEmpty(req.body)) res.sendStatus(404);
  else res.status(200).json(req.body);
});


const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`App is ready for requests on port ${PORT}!`)
});


