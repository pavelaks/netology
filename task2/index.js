/**
 * Created by pavelaks on 19/09/2017.
 */
const {ChatApp} = require('./chat');
const {chatOnMessage, chatOnMessagePrepare} = require('./chat');

let webinarChat = new ChatApp('webinar');
let facebookChat = new ChatApp('[fb]');
let vkChat = new ChatApp('[vk]');

vkChat.setMaxListeners(2);

webinarChat.on('message', chatOnMessagePrepare);
webinarChat.on('message', chatOnMessage);

facebookChat.on('message', chatOnMessage);

vkChat.on('message', chatOnMessagePrepare);
vkChat.on('message', chatOnMessage);
vkChat.once('close', () => {
  vkChat.emit('message', {chat: '[vk]', text: 'Чатик Вконтакте закрылся :('})
});


// Закрыть вконтакте
setTimeout(() => {
  vkChat.close();
}, 10000);


// Закрыть фейсбук
setTimeout(() => {
  facebookChat.close();
}, 15000);

// Закрыть фейсбук
setTimeout(() => {
  webinarChat.close();
}, 30000);
