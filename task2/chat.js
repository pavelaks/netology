const EventEmitter = require('events');

module.exports.chatOnMessage = chatOnMessage = (message) => {
  console.log(`${message.chat}: ${message.text}`);
};

module.exports.chatOnMessagePrepare = (message) => {
  chatOnMessage({chat: message.chat, text: 'Готовлюсь к ответу'});
  //chatOnMessage(message);
};

module.exports.ChatApp = class ChatApp extends EventEmitter {
  /**
   * @param {String} title
   */
  constructor(title) {
    super();

    this.title = title;

    // Посылать каждую секунду сообщение
    this.interval = setInterval(() => {
      this.emit('message', {chat: this.title, text: `ping-pong`});
    }, 1000);
  }

  close() {
    //this.emit('message', `Чатик ${this.title} закрылся :(`);
    this.emit('close');
    this.removeListener('message', chatOnMessage);
    /*
     Наверное и chatOnMessagePrepare тоже нужно удалить...
     Решил пока просто остановить генератор...
     */
    clearInterval(this.interval);
  }
};