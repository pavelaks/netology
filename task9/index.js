const express = require("express");
const app = express();
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({extended: false});

const mongoClient = require('mongodb').MongoClient;

// Connection URL  32768 - port from dev Docker Container...
let url = process.env.MONGO_URL || 'mongodb://localhost:32768/task9';

// Use connect method to connect to the Server
mongoClient.connect(url, function (err, db) {
  if (err) {
    console.log(`Mongo Client: Connection Error: ${err}`);
  } else {
    console.log("Mongo Client: Connected correctly to server");
    //db.close();

    // GENERAL
    app.use(urlencodedParser);

    require('./contacts_routes')(app, db);

    app.listen(process.env.PORT || 3000, () => {
      console.log(`Web Server is ready on port ${process.env.PORT || 3000}`);
    });

  }
});


//const contactsStorage = require('./contacts_handlers');

