module.exports = class contactsModel {

  constructor(db) {
    this.collection = db.collection('contacts');
  }

  createContact(data) {
    return new Promise((done, fail) => {
      let doc = data; //We should do some checks against injections... i think. Now it's not so safe =(
      this.collection.insertOne(doc, (err, result) => {
        if (err) {
          fail(err);
        } else {
          done(result.ops[0]);
        }
      });
    });


  }

  getContactList(fields = false, limit = 100, offset = 0) {
    return new Promise((done, fail) => {
      this.collection.find(fields).limit(limit).skip(offset).toArray((err, result) => {
        if (err) {
          fail(err);
        } else {
          done(result);
        }
      });
    });
  }

  getContact(contactId) {
    return new Promise((done, fail) => {
      let query = {"_id": contactId};
      this.collection.findOne(query, (err, result) => {
        if (err) {
          fail(err);
        } else {
          done(result);
        }
      });
    });
  }

  deleteContact(contactId) {
    return new Promise((done, fail) => {
      let query = {"_id": contactId};
      this.collection.deleteOne(query, (err, result) => {
        if (err) {
          fail(err);
        } else {
          done(result);
        }
      });
    });

  }

  updateContact(contactId, data) {
    return new Promise((done, fail) => {
      let query = {"_id": contactId};
      this.collection.updateOne(query, data, (err, result) => {
        if (err) {
          fail(err);
        } else {
          done(result);
        }
      });
    });

  }


  deleteAllContacts() {
    return new Promise((done, fail) => {
      let query = {};
      this.collection.deleteMany(query, (err, result) => {
        if (err) {
          fail(err);
        } else {
          done(result);
        }
      });
    });
  }

};