/*
Создать приложение на Express.js «Записная книжка» в виде API:

Добавить контакт (имя, фамилия, телефон);
Список добавленных контактов;
Удалить контакт;
Редактировать контакт;
Поиск по номеру телефона или фамилии или имени.
 */

const express = require('express');

module.exports = (app, db) => {

  const ContactsModel = require('./contacts_model');
  let Contacts = new ContactsModel(db);

  //REST
  const REST_API = express.Router();
  const ObjectID = require('mongodb').ObjectID;


  REST_API.param('id', (req, res, next, id) => {
    console.log(`CONTACT ID = ${id}`);
    if (!ObjectID.isValid(id)) res.status(400).send('Bad request. Check ID');
    req.id = new ObjectID(id);
    next();
  });

  REST_API.get("/", function (req, res) {

    let listLimit = parseInt(req.query.limit) || 0;
    let listOffset = parseInt(req.query.offset) || 0;
    let listPhoneFilter = req.query.phone || false;
    let listEmailFilter = req.query.email || false;

    let listFieldsFilter = {};
    if (listPhoneFilter) listFieldsFilter.phone = {$regex: listPhoneFilter};
    if (listEmailFilter) listFieldsFilter.email = {$regex: listEmailFilter};
    console.log(`REST GET CONTACTS LIST`);

    Contacts.getContactList(listFieldsFilter, listLimit, listOffset)
      .then(contactList => {
        if (!contactList.length) res.sendStatus(404);
        else res.json(contactList);
      })
      .catch((error) => {
        console.log(`Find error: ${error}`);
        res.sendStatus(500);
      });
  });


  REST_API.post("/", function (req, res) {
    console.log(`REST POST NEW CONTACT TO COLLECTION (data: ${req.body.firstName})`);
    let data = req.body;
    Contacts.createContact(data)
      .then(result => {
        res.json(result);
      })
      .catch(error => {
        console.log(`Insert error: ${error}`);
        res.sendStatus(500);
      });
  });

  REST_API.get("/:id", function (req, res) {
    console.log(`REST GET USER ${req.id} FROM LIST`);
    Contacts.getContact(req.id)
      .then(contactList => {
        if (!contactList) res.sendStatus(404);
        else res.json(contactList);
      })
      .catch((error) => {
        console.log(`Find error: ${error}`);
        res.sendStatus(500);
      });

  });
  REST_API.put("/:id", function (req, res) {
    console.log(`REST PUT UPDATE CONTACT (${req.id}) IN COLLECTION `);
    let data = req.body;

    Contacts.updateContact(req.id, data)
      .then(result => {
        console.log(result);
        if (result.result.n) return Contacts.getContact(req.id);
        else res.sendStatus(404);
      })
      .then(result => {
        res.json(result);
      })
      .catch(error => {
        console.log(`Update error: ${error}`);
        res.sendStatus(500);
      });

  });

  REST_API.delete("/:id", function (req, res) {
    console.log(`REST DELETE ${req.id} CONTACT FROM COLLECTION`);
    Contacts.deleteContact(req.id)
      .then(result => {
        res.sendStatus(204);
      })
      .catch(error => {
        console.log(`Delete One error: ${error}`);
        res.sendStatus(500);
      });
  });

  REST_API.delete("/", function (req, res) {
    console.log(`REST DELETE ALL CONTACTS FROM COLLECTION`);

    Contacts.deleteAllContacts()
      .then(result => {
        res.json(result);
      })
      .catch(error => {
        console.log(`Delete All error: ${error}`);
        res.sendStatus(500);
      });
  });

  app.use('/contacts', REST_API);

};