const fs = require('fs'), opts = {encoding: 'utf-8'};
const getRandomNumber = require('./random');
const numberOfSockets = 10;
const maxNumberOfPokemons = 3;

const hide = (path, pokemonList) => {
  //Pick Pokemons
  let maxPokemonNumber = pokemonList.length - 1;
  let pokemonShortList = [];
  let socketList = [];
  let playgroundSockets = [];

  for (let i = 0; i < maxNumberOfPokemons && i < maxPokemonNumber; i++) {
    let pokemon = pokemonList[getRandomNumber(0, maxPokemonNumber)];
    if (pokemonShortList.includes(pokemon)) i--; //Pokemon is already picked... Choose the next one
    else {
      let socketNumber = getRandomNumber(1, numberOfSockets);
      while (socketList.some(elem => elem.number === socketNumber)) //Regenerate SocketNumber
        socketNumber = getRandomNumber(1, numberOfSockets);
      socketList.push({path: path + '/' + leadingZero(socketNumber), pokemon, number: socketNumber});
    }
  }

  playgroundSockets = generateFieldDirPaths(path, numberOfSockets);

  //Create Playground & FillSockets (Dirs)
  return mkdir(path)
    .then(path => Promise.all(playgroundSockets.map(mkdir))) //mkdirs 01,02..10
    .then(path => Promise.all(socketList.map(placePokemon))) //Place Pokemons from pockemonShortList to

};

const seek = (path) => {
  /*Plan
   ParseField  check for  01/pokemon.txt, 02/pokemon.txt, 03/pokemon.txt & etc
   */

  let playgroundSockets = generateFieldDirPaths(path, numberOfSockets);

  return Promise.all(playgroundSockets.map(checkPokemon))
    .then(pokemonList => parseSockets(pokemonList))

};

const leadingZero = n => "0".substring(n > 9) + n;

const mkdir = path => {
  return new Promise((done, fail) => {
    fs.mkdir(path, err => {
      if (err) fail(err);
      else done(path)
    });
  });
};

const generateFieldDirPaths = (path, dir_number) => {
  let playground = [];
  while (dir_number > 0) {
    playground.push(path + '/' + leadingZero(dir_number--));
  }
  return playground;

};

const placePokemon = (socket) => {
  return new Promise((done, fail) => {
    let socketPath = socket.path + '/pokemon.txt';
    let socketData = socket.pokemon.name + '|' + socket.pokemon.attack + '|' + socket.pokemon.defense;
    fs.writeFile(socketPath, socketData, opts, err => {
      if (err) fail(err);
      else done(`Pokemon (${socketData}) is hided in socket ${socketPath}`);
    });
  });
};

const checkPokemon = (socket) => {
  return new Promise((done, fail) => {
    let socketPath = socket + '/pokemon.txt';
    fs.readFile(socketPath, opts, (err, data) => {
      if (err && err.code === 'ENOENT') done(0); //Suggest there is no pokemon here
      else if (err) fail(err);
      else done(data);
    });

  });
};

const parseSockets = (rawPokemonList) => {
  return new Promise((done, fail) => {
    let filteredPokemonList = rawPokemonList.filter((elem) => {
      return elem;
    });
    if (!filteredPokemonList.length) fail('There is no Pokemons');

    filteredPokemonList = filteredPokemonList.map((elem) => {
      let values = elem.split("|");
      return {name: values[0], attack: values[1], defense: values[2]}
    });
    done(filteredPokemonList);


  });
};

module.exports = {
  hide, seek
};


