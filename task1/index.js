const {hide} = require('./hidenseek');
const {seek} = require('./hidenseek');
const pathInfo = require('./path-info');

let argvAction = (process.argv[2] || false);
let fieldPath = (process.argv[3] || './field');
let pokemonListPath = (process.argv[4] || './pokemons.json');
//TODO FileStatus check for pokemonListPath
const pokemonList = require(pokemonListPath);

switch (argvAction) {
  case 'hide':
    console.log(`Let's try to hide ${pokemonListPath} in ${fieldPath}`);
    hide(fieldPath, pokemonList)
      .then(result => console.log(result))
      .catch(error => console.error(error));
    break;
  case 'seek':
    console.log(`Let's try to seek Pokemons in ${fieldPath}`);
    seek(fieldPath)
      .then(result => console.log(result))
      .catch(error => console.error(error));
    break;
  case 'info':
    //TASK 1.3
    pathInfo((process.argv[3] || '.'), showInfo);
    break;
  default:
    console.log('Sorry. We meet unknown action');
}


//TASK 1.3
function showInfo(err, info) {
  if (err) {
    console.log('Возникла ошибка при получении информации');
    return;
  }

  switch (info.type) {
    case 'file':
      console.log(`${info.path} — является файлом, содержимое:`);
      console.log(info.content);
      console.log('-'.repeat(10));
      break;
    case 'directory':
      console.log(`${info.path} — является папкой, список файлов и папок в ней:`);
      info.childs.forEach(name => console.log(`  ${name}`));
      console.log('-'.repeat(10));
      break;
    default:
      console.log('Данный тип узла не поддерживается');
      break;
  }
}